//
//  RepositoryTableViewCell.swift
//  DesafioConcrete
//
//  Created by Marcos on 23/07/17.
//  Copyright © 2017 Alessandro. All rights reserved.
//

import UIKit
import SDWebImage

class RepositoryTableViewCell: UITableViewCell
{

    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDescription: UILabel!
    @IBOutlet weak var labelTotalForks: UILabel!
    @IBOutlet weak var labelTotalLikes: UILabel!
    @IBOutlet weak var labelUsername: UILabel!
    @IBOutlet weak var labelFullname: UILabel!
    @IBOutlet weak var imageViewAvatar: UIImageView!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
    func setData(data:Repository!)
    {
        if data != nil
        {
            labelName.text = data.Name
            labelDescription.text = data.Desc
            labelTotalForks.text = String(describing: data.ForksCount)
            labelTotalLikes.text = String(describing: data.StargazersCount)
            labelUsername.text = data.Login
            labelFullname.text = data.Fullname
            imageViewAvatar.sd_setImage(with: URL(string: data.AvatarUrl!), placeholderImage: UIImage(named: "ic_exclamation"))
            imageViewAvatar.layer.masksToBounds = false
            imageViewAvatar.layer.cornerRadius = imageViewAvatar.frame.width / 2
            imageViewAvatar.layer.masksToBounds = true
        }
    }

}
