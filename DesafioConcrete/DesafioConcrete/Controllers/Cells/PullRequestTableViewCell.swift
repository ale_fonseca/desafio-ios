//
//  PullRequestTableViewCell.swift
//  DesafioConcrete
//
//  Created by Marcos on 23/07/17.
//  Copyright © 2017 Alessandro. All rights reserved.
//

import UIKit

class PullRequestTableViewCell: UITableViewCell {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelDescription: UILabel!
    @IBOutlet weak var labelUsername: UILabel!
    @IBOutlet weak var labelFullname: UILabel!
    @IBOutlet weak var imageViewAvatar: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(data:PullRequest!)
    {
        self.labelTitle.text = data.Title
        self.labelDescription.text = data.Body
        self.labelUsername.text = data.Login
        self.labelFullname.text = data.FullName
        imageViewAvatar.sd_setImage(with: URL(string: data.AvatarUrl!), placeholderImage: UIImage(named: "ic_exclamation"))
        imageViewAvatar.layer.masksToBounds = false
        imageViewAvatar.layer.cornerRadius = imageViewAvatar.frame.width / 2
        imageViewAvatar.layer.masksToBounds = true
    }

}
