//
//  PullRequestHeaderView.swift
//  DesafioConcrete
//
//  Created by Marcos on 23/07/17.
//  Copyright © 2017 Alessandro. All rights reserved.
//

import UIKit

class PullRequestHeaderView: UIView {

    @IBOutlet weak var labelOpened: UILabel!
    @IBOutlet weak var labelClosed: UILabel!
    
    func setData(data:PullRequest!)
    {
        self.labelOpened.text = String(describing: data.OpenIssues)
        self.labelClosed.text = String(describing: data.ClosedIssues)
    }

}
