//
//  GitHubJavaPopTableViewController.swift
//  DesafioConcrete
//
//  Created by Marcos on 20/07/17.
//  Copyright © 2017 Alessandro. All rights reserved.
//

import UIKit
import SDWebImage
import MBProgressHUD

class GitHubJavaPopTableViewController: UITableViewController
{

    var hud : MBProgressHUD = MBProgressHUD()
    
    let cellReuseIdentifier = "cell"
    let api = GitHubJavaAPI()
    
    var isLoading = false
    var repositories = [Repository]()
    var page = 1
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.setupUI()
        self.getRepositories()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.repositories.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:RepositoryTableViewCell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! RepositoryTableViewCell
        let repository:Repository = repositories[indexPath.row]
        cell.setData(data: repository)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        let repository:Repository = repositories[indexPath.row]
        getPullRequests(owner: repository.Login!, repository: repository.Name!)
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        let currentOffset = scrollView.contentOffset.y;
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        
        if maximumOffset - currentOffset > 1.0 {
            return
        }
        
        if !self.isLoading
        {
            self.getRepositories()
        }
    }
    
    // MARK: Service called
    
    func getRepositories()
    {
        self.hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        self.hud.mode = MBProgressHUDMode.indeterminate
        self.hud.label.text = "Loading"
        
        self.isLoading = true
        api.getRepositories(page: self.page) { (results) in
            
            for repositoryItem in results
            {
                self.repositories.append(repositoryItem)
            }
            
            self.tableView.reloadData()
            
            self.hud.hide(animated: true)
            self.page += 1
            self.isLoading = false
        }
    }
    
    func getPullRequests(owner: String, repository: String)
    {
        self.hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        self.hud.mode = MBProgressHUDMode.indeterminate
        self.hud.label.text = "Loading"
        
        api.getPullRequests(owner: owner, repository: repository) { (result) in
            self.hud.hide(animated: true)
            self.openRepositoryDetailsTableViewController(name: repository, data: result)
        }
    }
    
    func openRepositoryDetailsTableViewController(name:String, data:[PullRequest])
    {
        let viewController:RepositoryDetailsTableViewController = (self.storyboard?.instantiateViewController(withIdentifier: "repositoryDetailsTableViewController") as? RepositoryDetailsTableViewController)!
        viewController.pullRequests = data
        viewController.repositoryName = name
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func setupUI()
    {
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController?.navigationBar.barTintColor = UIColor(hex: "343438")
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [NSFontAttributeName: UIFont(name:"HelveticaNeue-Light", size:20)!,
                                                                        NSForegroundColorAttributeName : UIColor.white]
    }
}
