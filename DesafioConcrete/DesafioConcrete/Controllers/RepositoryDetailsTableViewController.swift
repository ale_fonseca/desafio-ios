//
//  RepositoryDetailsTableViewController.swift
//  DesafioConcrete
//
//  Created by Marcos on 21/07/17.
//  Copyright © 2017 Alessandro. All rights reserved.
//

import UIKit

class RepositoryDetailsTableViewController: UITableViewController {

    var repositoryName:String = ""
    var pullRequests:[PullRequest] = []
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.title = repositoryName
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.pullRequests.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:PullRequestTableViewCell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! PullRequestTableViewCell
        let pullRequest:PullRequest = pullRequests[indexPath.row]
        cell.setData(data: pullRequest)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        let pullRequest:PullRequest = pullRequests[indexPath.row]
        UIApplication.shared.openURL(URL(string: pullRequest.HtmlUrl!)!)
    }
    
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let headerView = UINib(nibName: "PullRequestHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! PullRequestHeaderView
        return headerView
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 44.0
    }
}
