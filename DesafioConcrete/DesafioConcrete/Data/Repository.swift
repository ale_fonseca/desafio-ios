//
//  Repository.swift
//  DesafioConcrete
//
//  Created by Marcos on 21/07/17.
//  Copyright © 2017 Alessandro. All rights reserved.
//

import UIKit

class Repository: NSObject
{
    static let ID = "id"
    static let NAME = "name"
    static let FULL_NAME = "full_name"
    static let AVATAR_URL = "avatar_url"
    static let DESC = "description"
    static let STARGAZERS_COUNT = "stargazers_count"
    static let FORKS_COUNT = "forks_count"
    static let LOGIN = "login"
    
    var Id: Int = 0
    var Name: String?
    var Fullname: String?
    var AvatarUrl: String?
    var StargazersCount: Int = 0
    var Desc: String?
    var ForksCount: Int = 0
    var Login: String?
}

