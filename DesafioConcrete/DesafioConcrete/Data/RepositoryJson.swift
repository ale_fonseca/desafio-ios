//
//  RepositoryJson.swift
//  DesafioConcrete
//
//  Created by Marcos on 21/07/17.
//  Copyright © 2017 Alessandro. All rights reserved.
//

import UIKit

class RepositoryJson: NSObject
{
    func getRepositoriesFromJson(_ data: Data) -> [Repository]
    {
        var repositories: [Repository] = []
        
        do {
            let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String:Any]
            if let items = json["items"] as? [[String: AnyObject]]
            {
                for item in items
                {
                    let repository:Repository = Repository()
                    
                    if let idRepository = item[Repository.ID] as? Int
                    {
                        repository.Id = idRepository
                    }
                    if let name = item[Repository.NAME] as? String
                    {
                        repository.Name = name
                    }
                    if let fullname = item[Repository.FULL_NAME] as? String
                    {
                        repository.Fullname = fullname
                    }
                    if let login = item["owner"]?[Repository.LOGIN] as? String
                    {
                        repository.Login = login
                    }
                    if let avatarUrl = item["owner"]?[Repository.AVATAR_URL] as? String
                    {
                        repository.AvatarUrl = avatarUrl
                    }
                    if let forksCount = item[Repository.FORKS_COUNT] as? Int
                    {
                        repository.ForksCount = forksCount
                    }
                    if let desc = item[Repository.DESC] as? String
                    {
                        repository.Desc = desc
                    }
                    if let stargazersCount = item[Repository.STARGAZERS_COUNT] as? Int
                    {
                        repository.StargazersCount = stargazersCount
                    }
                    
                    repositories.append(repository)
                }
            }

            
        } catch let error as NSError {
            debugPrint(error)
        }
        
        return repositories
    }
}
