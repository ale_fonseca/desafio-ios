//
//  PullRequestJson.swift
//  DesafioConcrete
//
//  Created by Marcos on 21/07/17.
//  Copyright © 2017 Alessandro. All rights reserved.
//

import UIKit

class PullRequestJson: NSObject
{
    func getPullRequestsFromJson(_ data: Data) -> [PullRequest]
    {
        var pullRequests: [PullRequest] = []
        
        do {
            
            let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [[String: AnyObject]]
            for pullRequestJson in json
            {
                let pullRequest:PullRequest = PullRequest()
                
                if let idPullRequest = pullRequestJson["user"]?[PullRequest.ID] as? Int
                {
                    pullRequest.Id = idPullRequest
                }
                if let login = pullRequestJson["user"]?[PullRequest.LOGIN] as? String
                {
                    pullRequest.Login = login
                }
                
                if let repo = pullRequestJson["head"]?["repo"] as? [String: Any]
                {
                    if let name = repo[PullRequest.NAME] as? String
                    {
                        pullRequest.Name = name
                    }
                    if let fullName = repo[PullRequest.FULL_NAME] as? String
                    {
                        pullRequest.FullName = fullName
                    }
                }
                
                if let title = pullRequestJson[PullRequest.TITLE] as? String
                {
                    pullRequest.Title = title
                }
                if let avatarUrl = pullRequestJson["user"]?[PullRequest.AVATAR_URL] as? String
                {
                    pullRequest.AvatarUrl = avatarUrl
                }
                if let body = pullRequestJson[PullRequest.BODY] as? String
                {
                    pullRequest.Body = body
                }
                if let openIssues = pullRequestJson["milestone"]?[PullRequest.OPEN_ISSUES] as? Int
                {
                    pullRequest.OpenIssues = openIssues
                }
                if let closedIssues = pullRequestJson["milestone"]?[PullRequest.CLOSED_ISSUES] as? Int
                {
                    pullRequest.ClosedIssues = closedIssues
                }
                if let htmlUrl = pullRequestJson[PullRequest.HTML_URL] as? String
                {
                    pullRequest.HtmlUrl = htmlUrl
                }
                pullRequests.append(pullRequest)
            }
        } catch let error as NSError {
            debugPrint(error)
        }
        
        return pullRequests
    }
}
