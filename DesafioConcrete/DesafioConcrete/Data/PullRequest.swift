//
//  PullRequest.swift
//  DesafioConcrete
//
//  Created by Marcos on 21/07/17.
//  Copyright © 2017 Alessandro. All rights reserved.
//

import UIKit

class PullRequest: NSObject
{
    static let ID = "id"
    static let NAME = "name"
    static let FULL_NAME = "full_name"
    static let TITLE = "title"
    static let LOGIN = "login"
    static let AVATAR_URL = "avatar_url"
    static let BODY = "body"
    static let OPEN_ISSUES = "open_issues"
    static let CLOSED_ISSUES = "closed_issues"
    static let HTML_URL = "html_url"
    
    var Id:Int = 0
    var Name:String?
    var FullName:String?
    var Title:String?
    var Login:String?
    var AvatarUrl:String?
    var Body:String?
    var OpenIssues:Int = 0
    var ClosedIssues:Int = 0
    var HtmlUrl:String?
    
}
