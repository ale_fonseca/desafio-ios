//
//  BaseAPI.swift
//  DesafioConcrete
//
//  Created by Marcos on 21/07/17.
//  Copyright © 2017 Alessandro. All rights reserved.
//

import UIKit

class BaseAPI: NSObject
{
    let API_PATH: String = "https://api.github.com/"
    
    func getURL(_ urlSufix: String) -> String {
        
        let url: String = self.API_PATH + urlSufix
        return url
    }
    
    func getRepositoriesUrl(_ page: Int) -> String {
        return self.getURL("search/repositories?q=language:Java&sort=stars&page=\(page)")
    }
    
    func getPullRequestsUrl(_ owner: String, repository: String) -> String
    {
        return self.getURL("repos/\(owner)/\(repository)/pulls")
    }
}
