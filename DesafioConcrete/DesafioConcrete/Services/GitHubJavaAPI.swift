//
//  GitHubJavaAPI.swift
//  DesafioConcrete
//
//  Created by Marcos on 21/07/17.
//  Copyright © 2017 Alessandro. All rights reserved.
//

import UIKit

class GitHubJavaAPI: HttpService
{
    let baseAPI = BaseAPI()
    let repositoryJson = RepositoryJson()
    let pullRequestJson = PullRequestJson()
    
    // MARK: - Methods Services
    func getRepositories(page:Int, completion: (([Repository]) -> Void)!)
    {
        self.doGet(url: (baseAPI.getRepositoriesUrl(page))) { (data) in
            var repositories = [Repository]()
            repositories = self.repositoryJson.getRepositoriesFromJson(data)
            completion(repositories)
        }
    }
    
    func getPullRequests(owner: String, repository: String, completion: (([PullRequest]) -> Void)!)
    {
        self.doGet(url: (baseAPI.getPullRequestsUrl(owner, repository: repository))) { (data) in
            var pullRequests = [PullRequest]()
            pullRequests = self.pullRequestJson.getPullRequestsFromJson(data)
            completion(pullRequests)
        }
    }
}
