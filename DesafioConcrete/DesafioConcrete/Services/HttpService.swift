//
//  HttpService.swift
//  DesafioConcrete
//
//  Created by Marcos on 21/07/17.
//  Copyright © 2017 Alessandro. All rights reserved.
//

import UIKit
import Alamofire

class HttpService: NSObject
{
    let auth = "token e5488eda226921a6c4a3c49e5ec08e9304c4b5b6"
    let PersonalAccessTokens:String = "e5488eda226921a6c4a3c49e5ec08e9304c4b5b6"
    // MARK - Method GET
    
    func doGet(url:String, completion:((Data) -> Void)!)
    {
        let headers: HTTPHeaders = [
            "Authorization": auth,
            "User-Agent": "Awesome-Octocat-App",
            "Accept": "application/vnd.github.v3.full+json",
            "Content-Type": "application/json; charset=utf-8"
        ]
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        Alamofire.request(url, method: .get, headers: headers).responseData { response in
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            
            debugPrint(response.request!)  // original URL request
//            debugPrint(response.response!) // HTTP URL response
//            debugPrint(response.data!)     // server data
//            debugPrint(response.result)    // result of response serialization
            
            var result = Data()
            
//            if let returnData = String(data: response.result.value!, encoding: .utf8) {
//                debugPrint("returnData: \(returnData)")
//            }
            
            if let data = response.result.value
            {
//                let utf8Text = String(data: data, encoding: .utf8)
//                debugPrint("Data: \(utf8Text)")
                result = data
            }
            
            completion(result)
        }
    }
    
    // MARK: - Method POST
    func doPost(url: String, parameters: Parameters, completion: ((Data) -> Void)!) {
        
        let headers: HTTPHeaders = [
            "Authorization": auth,
            "Accept": "application/vnd.github.v3+json",
            "Content-Type": "application/json; charset=utf-8"
        ]
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        Alamofire.request(url, method: .post, parameters: parameters, headers: headers).responseData { response in
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            
            debugPrint(response.request!)  // original URL request
            debugPrint(response.response!) // HTTP URL response
            debugPrint(response.data!)     // server data
            debugPrint(response.result)    // result of response serialization
            
            var result = Data()
            
            if let data = response.result.value, let utf8Text = String(data: data, encoding: .utf8) {
                debugPrint("Data: \(utf8Text)")
                result = data
            }
            
            completion(result)
        }
    }
}
